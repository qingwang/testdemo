//
//  UserView.swift
//  TestDemo
//
//  Created by GDzqw on 2020/2/28.
//  Copyright © 2020 Mr.K. All rights reserved.
//

import UIKit

class UserView: UIView {
    
    private var item: UserItem = UserItem()

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.makeupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func updateWithItem(item: UserItem) {
        self.item = item
        
        if self.item.hdImageurl.count > 0 {
            self.avatarImageView.image = UIImage.init(named: self.item.hdImageurl)
        }
        self.starView.updateWithItem(item: item)
    }
    
    // UI
    private func makeupViews() {
        self.backgroundColor = UIColor.white
        
        self.addSubview(self.starView)
        self.starView.snp.makeConstraints {(make) in
            make.top.equalToSuperview().offset(80)
            make.centerX.equalToSuperview()
            make.height.equalTo(50)
            make.width.equalTo(200)
        }
        
        self.addSubview(self.avatarImageView)
        self.avatarImageView.snp.makeConstraints {(make) in
            make.top.equalTo(self.starView.snp.bottom).offset(5)
            make.centerX.equalToSuperview()
            make.height.equalTo(200)
            make.width.equalTo(200)
        }
        
        self.addSubview(self.closeButton)
    }
    
    // Action
    @objc private func onCloseAction() {
        self.isHidden = true
    }
    
    // Getter && Setter
    lazy var closeButton: UIButton = {
        let button: UIButton = UIButton()
        button.frame = CGRect.init(x: 15, y: 40, width: 50, height: 30)
        button.setTitle("Close", for: UIControl.State.normal)
        button.backgroundColor = UIColor.lightGray
        button.setTitleColor(UIColor.black, for: UIControl.State.normal)
        button.addTarget(self, action: #selector(onCloseAction), for: UIControl.Event.touchUpInside)
        return button
    }()
    
    lazy var avatarImageView: UIImageView = {
        let imageView: UIImageView = UIImageView()
        return imageView
    }()
    
    lazy var starView: StartView = {
        let view: StartView = StartView.init(frame: CGRect.init(x: 0, y: 50, width: 200, height: 50))
        return view
    }()

}
