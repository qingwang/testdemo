//
//  HomeView.swift
//  TestDemo
//
//  Created by GDzqw on 2020/2/28.
//  Copyright © 2020 Mr.K. All rights reserved.
//

import UIKit
import SnapKit

class HomeView: UIView {
    private var contentsItem: ContentsItem?
    private var completion: ((UserItem) -> Void)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.makeupUI()
    }
    
    convenience init(completion: @escaping (UserItem) -> Void) {
        self.init(frame: CGRect.zero)
        self.completion = completion
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func updateWithItem(item: ContentsItem) {
        self.contentsItem = item
        if item.contentArray.count <= 0 {
            // 空页面展示
        } else {
            self.titleLabel.text = item.title
            self.subLabel.text = item.subTitle
        }
    }
    
    // UI
    private func makeupUI() {
        self.backgroundColor = UIColor.white
        
        self.addSubview(self.titleLabel)
        self.titleLabel.snp.makeConstraints {(make) in
            make.top.equalToSuperview().offset(70)
            make.left.equalToSuperview().offset(0)
            make.right.equalToSuperview().offset(0)
            make.height.equalTo(30)
        }
        
        self.addSubview(self.subLabel)
        self.subLabel.snp.makeConstraints {(make) in
            make.top.equalTo(self.titleLabel.snp.bottom).offset(5)
            make.left.equalToSuperview().offset(15)
            make.right.equalToSuperview().offset(-15)
            make.height.equalTo(50)
        }
        
        self.addSubview(self.collectionView)
        self.collectionView.snp.makeConstraints {(make) in
            make.top.equalTo(self.subLabel.snp.bottom).offset(5)
            make.centerX.equalToSuperview()
            make.width.equalTo(230)
            make.bottom.equalToSuperview().offset(-20)
        }
    }
    
    // Getter && Setter
    lazy var titleLabel: UILabel = {
        let label: UILabel = UILabel()
        label.font = UIFont.systemFont(ofSize: 20)
        label.textColor = UIColor.black
        label.numberOfLines = 0
        label.textAlignment = .center
        return label
    }()
    
    lazy var subLabel: UILabel = {
        let label: UILabel = UILabel()
        label.font = UIFont.systemFont(ofSize: 16)
        label.textColor = UIColor.black
        label.numberOfLines = 0
        return label
    }()
    
    lazy var flowLayout: UICollectionViewFlowLayout = {
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.itemSize = CGSize(width: 100, height: 100)
        flowLayout.scrollDirection = UICollectionView.ScrollDirection.vertical
        return flowLayout
    }()
    
    lazy var collectionView: UICollectionView = {
        let collectionView:UICollectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: self.flowLayout)
        collectionView.backgroundColor = UIColor.white
        let identifier = String(describing: HomeCollectionCell.self)
        collectionView.register(UINib(nibName: identifier, bundle: nil), forCellWithReuseIdentifier: identifier)
        collectionView.delegate = self
        collectionView.dataSource = self
        
        return collectionView
    }()
}

extension HomeView: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let identifier = String(describing: HomeCollectionCell.self)
        let cell: HomeCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as? HomeCollectionCell ?? HomeCollectionCell()
        if let contentsItem = self.contentsItem,  indexPath.row < contentsItem.contentArray.count {
            let item: UserItem = contentsItem.contentArray[indexPath.row]
            cell.updateWithItem(item: item)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let contentsItem = self.contentsItem, indexPath.row < contentsItem.contentArray.count {
            let item: UserItem = contentsItem.contentArray[indexPath.row]
            if self.completion != nil {
                self.completion!(item)
            }
        }
    }
}
