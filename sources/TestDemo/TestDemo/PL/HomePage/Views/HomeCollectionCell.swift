//
//  HomeCollectionCell.swift
//  TestDemo
//
//  Created by GDzqw on 2020/3/1.
//  Copyright © 2020 Mr.K. All rights reserved.
//

import UIKit

class HomeCollectionCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var name: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    public func updateWithItem(item: UserItem) {
        if item.smallImageurl.count > 0 {
            self.imageView.image = UIImage.init(named: item.smallImageurl)
        }
        if item.name.count > 0 {
            self.name.text = item.name
        }
    }
}
