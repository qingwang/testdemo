//
//  StartView.swift
//  TestDemo
//
//  Created by GDzqw on 2020/3/1.
//  Copyright © 2020 Mr.K. All rights reserved.
//

import UIKit

class StartView: UIView {
    
    private var item: UserItem?
    private var starWidth: CGFloat = 0
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.makeupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func updateWithItem(item: UserItem) {
        self.item = item
        self.updateScoreUI()
    }
    
    public func clearAll(){
        for view in self.subviews{
            view.removeFromSuperview()
        }
        
        if let taps = self.gestureRecognizers {
            for tap in taps{
                self.removeGestureRecognizer(tap)
            }
        }
       
    }
    
    // UI
    private func makeupViews() {
        
        let count: NSInteger = 5
        self.starWidth = self.bounds.size.width/CGFloat(count)
        self.addSubview(self.bgView)
        self.addSubview(self.foreView)
        for index in 0...count-1 {
            let imageView: UIImageView = UIImageView(image: UIImage.init(named: "starBg"))
            imageView.frame = CGRect(x:CGFloat(index) * self.starWidth, y: 0, width: self.starWidth, height: self.bounds.size.height)
            imageView.contentMode = .scaleAspectFit
            self.bgView.addSubview(imageView)
        }
        
        for index in 0...count-1 {
            let imageView: UIImageView = UIImageView(image: UIImage.init(named: "starFore"))
            imageView.frame = CGRect(x:CGFloat(index) * self.starWidth, y: 0, width: self.starWidth, height: self.bounds.size.height)
            imageView.contentMode = .scaleAspectFit
            self.foreView.addSubview(imageView)
        }
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapStarAction(sender:)))
        self.addGestureRecognizer(tap)
    }
    
    private func updateScoreUI() {
        if let item = self.item {
            let animationTimeInterval = 0.2
            UIView.animate(withDuration: animationTimeInterval) {
                self.foreView.frame = CGRect(x: 0, y: 0, width: self.starWidth * CGFloat(item.score), height: self.bounds.size.height)
            }
        }
    }
    
    // Action
    @objc func tapStarAction(sender:UITapGestureRecognizer){
        let  tapPoint = sender.location(in: self)
        let  offset   = tapPoint.x
        let  selctCount = offset / self.starWidth
        if let item = self.item {
            item.score = NSInteger(ceilf(Float(selctCount)))
            self.updateScoreUI()
        }
    }
    
    // Getter && Setter
    private lazy var bgView: UIView = {
        let view: UIView = UIView()
        view.backgroundColor = UIColor.clear
        view.frame = self.bounds
        view.clipsToBounds = true
        return view
    }()
    
    private lazy var foreView: UIView = {
        let view: UIView = UIView()
        view.backgroundColor = UIColor.clear
        view.clipsToBounds = true
        view.frame = CGRect.zero
        return view
    }()
}
