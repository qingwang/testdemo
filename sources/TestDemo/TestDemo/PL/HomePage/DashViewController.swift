//
//  DashViewController.swift
//  TestDemo
//
//  Created by GDzqw on 2020/2/28.
//  Copyright © 2020 Mr.K. All rights reserved.
//

import UIKit

class DashViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.setupUI()
        self.setupAction()
        self.requestData()
    }
    
    // UI
    private func setupUI() {
        self.view.backgroundColor = UIColor.white
        self.view.addSubview(self.homeView)
        self.homeView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(0)
            make.left.equalToSuperview().offset(0)
            make.bottom.equalToSuperview().offset(0)
            make.width.equalTo(self.view.bounds.width)
        }
        
        self.view.addSubview(self.userView)
        self.userView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(0)
            make.left.equalToSuperview().offset(0)
            make.bottom.equalToSuperview().offset(0)
            make.width.equalTo(self.view.bounds.width)
        }
    }
    
    // Action
    private func setupAction() {
        NotificationCenter.default.addObserver(self, selector: #selector(handleDeviceOrientationChange(_:)), name: UIDevice.orientationDidChangeNotification, object: nil)
    }
    
    // Request
    private func requestData() {
        let model: ContentsModel = ImageService.default.getContentItem()
        let contentItem = ContentsItem.init(model)
        self.homeView.updateWithItem(item: contentItem)
        if contentItem.contentArray.count > 0 {
            let userItem: UserItem = contentItem.contentArray[0]
            self.userView.updateWithItem(item: userItem)
        }
    }
    
    // Delegate
    @objc private func handleDeviceOrientationChange(_ notification: NSNotification) {
        let orientation = UIDevice.current.orientation
        var isHideUserViewTag: Bool = true
        var viewWidth: CGFloat = self.view.bounds.size.width
        if viewWidth > self.view.bounds.size.height {
            viewWidth = viewWidth / 2
            isHideUserViewTag = false
        }
        
        switch orientation {
        case .landscapeRight, .landscapeLeft, .portraitUpsideDown:
            self.userView.isHidden = isHideUserViewTag
            self.homeView.snp.updateConstraints { (make) in
                make.width.equalTo(viewWidth)
            }
            self.userView.snp.updateConstraints { (make) in
                make.width.equalTo(viewWidth)
                make.left.equalToSuperview().offset(viewWidth)
            }
            break
        case .portrait :
            self.userView.isHidden = isHideUserViewTag
            self.homeView.snp.updateConstraints { (make) in
                make.width.equalTo(viewWidth)
            }
            self.userView.snp.updateConstraints { (make) in
                make.width.equalTo(viewWidth)
                make.left.equalToSuperview().offset(0)
            }
            break
        default: break
        }
    }
    
    // Getter && Setter
    lazy var homeView: HomeView = {
        let view = HomeView.init { [weak self] (userItem) in
            guard let self = self else { return }
            self.userView.updateWithItem(item: userItem)
            self.userView.isHidden = false
        }
        return view
    }()
    
    lazy var userView: UserView = {
        let view = UserView()
        return view
    }()
}
