//
//  ContentsData.swift
//  TestDemo
//
//  Created by GDzqw on 2020/3/1.
//  Copyright © 2020 Mr.K. All rights reserved.
//

import UIKit

class ContentsData: NSObject {
    
    var title = "Friendsr"
    var subTitle = "Click on an righible angle users to learn more and sec if you want to compatible for a date! Click on an righible angle users to learn more and sec if you want to compatible for a date!"
    var contentArray: String = ""
    
    //构造方法
    init(title:String = "", subTitle:String = "", contentArray: String = ""){
        self.title = title
        self.subTitle = subTitle
        self.contentArray = contentArray
        super.init()
    }
    
    //从nsobject解析回来
    init(coder aDecoder:NSCoder!) {
        self.title = aDecoder.decodeObject(forKey: "title") as? String ?? ""
        self.subTitle = aDecoder.decodeObject(forKey: "subTitle") as? String ?? ""
        self.contentArray = aDecoder.decodeObject(forKey: "contentArray") as? String ?? ""
    }

    //编码成object
    func encodeWithCoder(aCoder:NSCoder!){
        aCoder.encode(self.title,forKey:"title")
        aCoder.encode(self.subTitle,forKey:"subTitle")
        aCoder.encode(self.contentArray,forKey:"contentArray")
    }
}
