//
//  ImageClient.swift
//  TestDemo
//
//  Created by GDzqw on 2020/2/28.
//  Copyright © 2020 Mr.K. All rights reserved.
//

import UIKit

class ImageClient: NSObject {
    func requestContentItem() -> ContentsData {
        var data: ContentsData = ContentsData()
        
        if self.isNetworking() {
            self.setSaveData(data: data)
        } else {
            data = self.getSaveData()
        }
        return data
    }
    
    private func setSaveData(data: ContentsData) {
        // 保存20条请求数据数据，使得离线仍然有数据返回
        let data = NSMutableData()
        let archiver = NSKeyedArchiver(forWritingWith: data)
        archiver.encode(data, forKey: "contentData")
        archiver.finishEncoding()
        data.write(toFile: "", atomically: true)
    }
    
    private func getSaveData() -> ContentsData {
        // 离现时返回保存数据
        let path = ""
        let defaultManager = FileManager()
        if defaultManager.fileExists(atPath: path) {
            let data = NSData(contentsOfFile: path)
            if let data = data {
                let unarchiver = NSKeyedUnarchiver(forReadingWith: data as Data)
                let dataContent: ContentsData = unarchiver.decodeObject(forKey: "contentData") as? ContentsData ?? ContentsData()
                unarchiver.finishDecoding()
                return dataContent
            }
        }
        return ContentsData()
    }
    
    private func isNetworking() -> Bool {
        // 判断网络环境
        return true
    }
}
