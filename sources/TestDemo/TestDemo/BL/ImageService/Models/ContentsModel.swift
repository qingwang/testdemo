//
//  ContentsModel.swift
//  TestDemo
//
//  Created by GDzqw on 2020/2/28.
//  Copyright © 2020 Mr.K. All rights reserved.
//

import UIKit

class ContentsModel: NSObject {
    var title = ""
    var subTitle = ""
    var contentArray: NSArray = NSArray()
    func initWithData(data: ContentsData) {
        self.title = data.title
        self.subTitle = data.subTitle
    }

}
