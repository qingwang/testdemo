//
//  AppDelegate.swift
//  TestDemo
//
//  Created by GDzqw on 2020/2/28.
//  Copyright © 2020 Mr.K. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var winodw: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        self.makeupWindow()
        return true
    }
    
    private func makeupWindow() {
        self.winodw = UIWindow(frame: UIScreen.main.bounds)
        let rootVC = DashViewController()
        self.winodw?.rootViewController = rootVC
        self.winodw?.backgroundColor = .white
        self.winodw?.makeKeyAndVisible()
    }
}
