platform :ios, '8.0'

target 'TestDemo' do
  pod 'SDWebImage', '~> 4.3.2'
  pod 'SnapKit', '~> 4.2.0'

end